import React, { useState } from "react";
import CardAttributes from "../components/Attributes/CardAttributes";
import Card from "../components/Cards/Card";
import Modal from "../components/UI/Modals/Modal";
import Wrapper from "../components/Wrappers/Wrapper";
import { DeckProps } from "../types/types";

const labelStats = ["Archetype", "Name", "Type", "Level"];
const labelAttr = ["Attribute", "Atk", "Def", "Race"];

const Deck: React.FC<DeckProps> = ({ CardList }) => {
  const [modal, setModal] = useState<number>(-1);

  const onClickHandler = (CardId: number) => (event: React.MouseEvent<HTMLImageElement>) => {
    setModal(CardId);
  };

  const onCloseModalHandler = () => {
    setModal(-1);
  };

  return (
    <>
      {modal !== -1 && (
        <Modal
          onCloseModal={onCloseModalHandler}
          title="Card Properties"
          card={CardList[modal].card_images[(CardList[modal].card_images.length * Math.random()) | 0].image_url}
        >
          <Wrapper className="card-property-container">
            <Wrapper className="card-details stats">
              <Wrapper className="card-properties-container">
                {[CardList[modal].archetype, CardList[modal].name, CardList[modal].type, CardList[modal].level].map(
                  (attr, index) => {
                    return (
                      attr && (
                        <CardAttributes
                          key={index}
                          className="property-item"
                          label={`${labelStats[index]}:`}
                          value={attr}
                        ></CardAttributes>
                      )
                    );
                  }
                )}
              </Wrapper>
            </Wrapper>
            <Wrapper className="card-details attributes">
              {[CardList[modal].attribute, CardList[modal].atk, CardList[modal].def, CardList[modal].race].map(
                (attr, index) => {
                  return (
                    attr && (
                      <CardAttributes
                        key={index}
                        className="property-item"
                        label={`${labelAttr[index]}:`}
                        value={attr}
                      ></CardAttributes>
                    )
                  );
                }
              )}
            </Wrapper>
            <Wrapper className="card-details description">
              <CardAttributes
                className="property-item"
                label="Description:"
                value={CardList[modal].desc}
              ></CardAttributes>
            </Wrapper>
          </Wrapper>
        </Modal>
      )}
      <h1 className="title">My Card List</h1>
      <Wrapper className="card-wrapper">
        {CardList.map((card, index) => {
          return (
            <Wrapper className="card-preview" key={index}>
              <Card
                key={index}
                value={index}
                onClick={onClickHandler(index)}
                prevImg={card.card_images[0].image_url_small}
                title={card.name}
              />
            </Wrapper>
          );
        })}
      </Wrapper>
    </>
  );
};

export default Deck;
