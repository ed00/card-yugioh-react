import Wrapper from "../components/Wrappers/Wrapper";

const Duelist: React.FC = () => {
  return (
    <>
      <h1 className="title">Welcome!</h1>
      <Wrapper className="about-wrapper">
        <p className="about-me">
          Please allow me to introduce myself I'm a full-stack web developer I've been around for long, long years
          programming with soul and faith I was 'round programming when logo: the turtle had his moment of glory in
          Mexican schools made damn sure the turtle got safe to its destiny and sealed its fate.
        </p>
        <p className="about-me">
          Pleased to meet you, hope you remember my name and that you're not baffled by the nature of my code.
        </p>
      </Wrapper>
    </>
  )
}

export default Duelist