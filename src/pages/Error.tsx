export const Error404: React.FC = () => {
  return <h1 className="title">Page not found</h1>
}