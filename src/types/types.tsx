export type CardProps = {
  className?: string;
  prevImg: string;
  title: string;
  value?: number;
  onClick?: React.MouseEventHandler<HTMLImageElement>
};

export type CardAtrtributesProps = {
  className: string;
  label: string;
  value: number | string;
};

export type ModalOverlayProps = {
  onCloseModal: () => void;
  card: string;
  title: string;
  children: JSX.Element;
};

export type ModalProps = {
  onCloseModal: () => void;
  card: string;
  title: string;
  children: JSX.Element;
};

export type WrapperProps = {
  onClick?: () => void;
  className: string;
  children: React.ReactNode;
};

export type CardImages = {
  image_url: string;
  image_url_small: string;
};

export type CardType = {
  id: string;
  card_images: CardImages[];
  archetype: string;
  name: string;
  type: string;
  level: number;
  attribute: string;
  def: number;
  atk: number;
  race: string;
  desc: string;
};

export type DeckProps = {
  CardList: CardType[];
};
