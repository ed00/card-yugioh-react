import React from "react";
import { CardProps } from "../../types/types";
import "./Card.scss";

const Card: React.FC<CardProps> = ({ className = "", prevImg, title, value = -1, onClick }) => {
  return <img className={`yugi-card ${className}`} src={prevImg} alt={title} data-id={value} onClick={onClick} />;
};

export default Card