import { FcAbout } from "react-icons/fc";
import { Link } from "react-router-dom";
import Logo from "../../logo.svg";
import "./Navbar.scss";

const Navbar: React.FC = () => {
  return (
    <>
      <nav className="navbar">
        <ul className="menu">
          <li className="logo">
            <Link to="/">
              <img className="nav-logo" src={Logo} alt="Logo" />
            </Link>
          </li>
          <li>
            <Link to="/duelist">
              <button type="button" className="btn draw-border">
                Duelist: Edna&nbsp;<FcAbout />
              </button>
            </Link>
          </li>
        </ul>
      </nav>
    </>
  )
}

export default Navbar
