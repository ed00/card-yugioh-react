import { WrapperProps } from "../../types/types";
import "./Wrapper.scss";

const Wrapper: React.FC<WrapperProps> = ({ onClick, className, children }) => {
  return <div className={className} onClick={onClick}>{children}</div>
}

export default Wrapper