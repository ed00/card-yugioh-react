import { CardAtrtributesProps } from "../../types/types";

const CardAttributes: React.FC<CardAtrtributesProps> = ({ className,label,value }) => {
  return (
    <>
      <div className={className}>
        <p><b>{label}</b></p>
        <p>{value}</p>
      </div>
    </>
  )
}

export default CardAttributes