import ReactDOM from "react-dom";
import Card from "../../Cards/Card";
import Wrapper from "../../Wrappers/Wrapper";
import React from "react";
import { ModalOverlayProps, ModalProps } from "../../../types/types";

const ModalOverlay: React.FC<ModalOverlayProps> = ({ onCloseModal, card, title, children }) => {
  return (
    <>
      <Wrapper className="modal-wrapper" onClick={onCloseModal}>
        <Card className="yugi-card-modal card-preview-modal" prevImg={card} title={title} />
        <Wrapper className="card modal">
          <Wrapper className="card modal-header">
            <h1 className="title">{title}</h1>
            {children}
          </Wrapper>
        </Wrapper>
      </Wrapper>
    </>
  );
};

const Modal: React.FC<ModalProps> = ({ onCloseModal, card, title, children }) => {
  const portalElem = document.getElementById("overlay-root");

  return (
    <>
      {portalElem &&
        ReactDOM.createPortal(
          <ModalOverlay title={title} card={card} onCloseModal={onCloseModal}>
            {children}
          </ModalOverlay>,
          portalElem
        )}
    </>
  );
};

export default Modal
