import { useEffect, useState } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Navbar from "./components/Navbar/Navbar";
import Deck from "./pages/Deck";
import Duelist from "./pages/Duelist";
import { Error404 } from "./pages/Error";
import "./styles/App.scss";
import { CardType } from "./types/types";

const App: React.FC = () => {
  const [CardList, setCardList] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const request = await fetch(
        "https://db.ygoprodeck.com/api/v7/cardinfo.php?name=toadally%20awesome%7CCyber%20Dinosaur%7Cblue-eyes%20white%20dragon%7Ckuriboh",
        {
          method: "POST",
        }
      );
      if (request.ok) {
        const response = await request.json();
        response.data.map((Card: CardType) => {
          Card.card_images.sort(() => Math.random() - Math.random()).find(() => true);
          return Card;
        });
        setCardList(response.data);
      } else {
        console.log("Fail: fetch data");
      }
    };
    fetchData();
  }, []);

  return (
    <Router>
      <Navbar />
      <Switch>
        <Route path="/duelist">
          <Duelist />
        </Route>
        <Route exact path="/">
          {CardList.length !== 0 && <Deck CardList={CardList} /> }
        </Route>
        <Route path="*">
          <Error404 />
        </Route>
      </Switch>
    </Router>
  )
}

export default App
